<?php
/*
Plugin Name: Ngrecord Plugin
Plugin URI: http://www.oscarmora.com.co
Description: None.
Version: 1.0
Author: Oscar Mora
Author URI: http://www.oscarmora.com.co
License: EULA
*/

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

if (!class_exists('Services_JSON')) {
  $dir = ngrecord_dir();
  require_once "$dir/lib/JSON.php";
}

$dir = ngrecord_dir();

function ngrecord_dir() {
  if (defined('NGRECORD_DIR') && file_exists(NGRECORD_DIR)) {
    return NGRECORD_DIR;
  } else {
    return dirname(__FILE__);
  }
}

add_action('init', 'init_ngrecord_post_type');

function init_ngrecord_post_type(){
  new ngrecord_post_type();
}

/**
* Post Type
*/
class ngrecord_post_type
{
  public function __construct()
  {
    $this->registerPostType();
  }

  public function registerPostType()
  {
    $args=array(
      'labels'=> array(
        'name'=>'Ngrecord Post Portfolio',
        'singular_name'=>'Post Portfolio',
        'add_new'=>'Add New Post Portfolio',
        'add_new_item'=>'Add New Post Portfolio Item'
        ),
      'query_var'=>'portfolio',
      'rewrite'=>array(
        'slug'=>'portfolio/'
        ),
      'public'=>true,
      'supports'=>array(
        'title',
        'editor',
        'thumbnail',
        'excerpt',
        )
      );

    register_post_type('ngrecord_post', $args);

    $args=array(
      'labels'=> array(
        'name'=>'Ngrecord Post Blog',
        'singular_name'=>'Post Blog',
        'add_new'=>'Add New Post Blog',
        'add_new_item'=>'Add New Post Blog Item'
        ),
      'query_var'=>'blog',
      'rewrite'=>array(
        'slug'=>'blog/'
        ),
      'public'=>true,
      'supports'=>array(
        'title',
        'editor',
        'thumbnail',
        'excerpt'
        )
      );

    register_post_type('ngrecord_blog', $args);
  }
}

add_action('wp_ajax_contact', 'ngrecord_contact');
add_action('wp_ajax_nopriv_contact', 'ngrecord_contact');

function ngrecord_contact(){

    if ( !wp_verify_nonce( $_REQUEST['nonce'], "contact_nonce")) {
      exit('{"error":"Nonce no valido"}');
    }  


  $email = $_REQUEST['email'];
  $message = $_REQUEST['message'];
  $name = $_REQUEST['name'];
  $subject = $_REQUEST['subject'];
  $toEmail = get_option('admin_email');

  $headers = "From: $name <$email>" . "\r\n";
  $result = wp_mail($toEmail, $subject, $message, $headers);
  
  exit('{"success":"Mensaje Enviado"}');
}

add_action('wp_ajax_contact_nonce', 'ngrecord_contact_nonce');
add_action('wp_ajax_nopriv_contact_nonce', 'ngrecord_contact_nonce');

function ngrecord_contact_nonce(){
  $nonce = wp_create_nonce("contact_nonce");

  echo('{"nonce":"'.$nonce.'"}');
  exit;
}

add_filter('json_api_controllers', 'attachment');

function attachment($controllers) {
  // Corresponds to the class JSON_API_MyController_Controller
  $controllers[] = 'attachment';
  return $controllers;
}

// Register the source file for JSON_API_Widgets_Controller
add_filter('json_api_attachment_controller_path', 'attachment_controller_path');

function attachment_controller_path($default_path) {
  return __FILE__;
}

class JSON_API_Attachment_Controller {

  public function get() {
    global $json_api;
    $id = $json_api->query->id;
    $attachments = get_post_meta($id, 'attachments', true);
    $result = array();

    if ($attachments) {
      $json = new Services_JSON();
      $attachments = $json->decode($attachments)->attachments;
      foreach ($attachments as $key => $value) {
        $result['attachments'][] = array(
          'id' => $value->id,
          'url' => wp_get_attachment_url($value->id),
          'title' => $value->fields->title,
          'caption' => $value->fields->caption
          );
      }
    }

    return $result;
  }
}

/*add_filter('json_api_encode', 'json_encode_attachment');

function json_encode_attachment($response) {
  return $response; 
}*/

// Disable get_author_index method (e.g., for security reasons)
/*add_action('json_api-attachment-get', 'action_attachment_get');

function action_attachment_get() {
  echo "Stop execution";
  exit;
}*/