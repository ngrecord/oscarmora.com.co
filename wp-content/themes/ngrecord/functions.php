<?php

if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'name'          => __( 'Title Sidebar', 'ngrecord' ),
		'id'            => 'sidebar-title',
		'description'   => __( '', 'ngrecord' ),
	    'before_widget' => '',
	    'after_widget' => '',
	    'before_title' => '<div class="title">',
	    'after_title' => '</div>',
));

add_action('after_setup_theme', 'ngrecord_setup');

function ngrecord_setup(){
	add_theme_support('post-thumbnails');
	set_post_thumbnail_size( 624, 9999 );
}

function ngrecord_init() {
	if (!is_admin()) {
		wp_enqueue_script('jQuery', get_bloginfo('template_url') . '/js/jquery-1.8.3.js', null, '1.8.3', true);
		wp_enqueue_script('Modernizr', get_bloginfo('template_url') . '/js/modernizr.custom.56113.js', array('jQuery'), '2.6.2', true);
		wp_enqueue_script('UA', get_bloginfo('template_url') . '/js/ua.js', array('jQuery'), '0.6.4', true);
		//wp_enqueue_script('Require', get_bloginfo('template_url') . '/js/require.js', array('jQuery'), '1.3', true);
		
		//*wp_enqueue_script('jQueryEasing', get_bloginfo('template_url') . '/js/jquery.easing.1.3.js', array('jQuery'), '1.3', false);
		//*wp_enqueue_script('Mousewheel', get_bloginfo('template_url') . '/js/jquery.mousewheel.js', array('jQuery'), '1.0.0', false);
		//*wp_enqueue_script('EventDrag', get_bloginfo('template_url') . '/js/jquery.event.drag-2.2.js', array('jQuery'), '2.2', false);
		//*wp_enqueue_script('Moment', get_bloginfo('template_url') . '/js/moment.js', array('jQuery'), '1.7.2', false);
		//*wp_enqueue_script('MomentLangEs', get_bloginfo('template_url') . '/js/moment.lang.es.js', array('jQuery', 'Moment'), '1.7.2', false);
		//*wp_enqueue_script('Masonry', get_bloginfo('template_url') . '/js/jquery.masonry.js', array('jQuery'), '2.1.0', false);
		//*wp_enqueue_script('Bookblock', get_bloginfo('template_url') . '/js/jquery.bookblock.js', array('jQuery'), '1.0.3', false);
		//*wp_enqueue_script('Baraja', get_bloginfo('template_url') . '/js/jquery.baraja.js', array('jQuery'), '1.0.0', false);
		//*wp_enqueue_script('Sharrre', get_bloginfo('template_url') . '/js/jquery.sharrre-1.3.4.js', array('jQuery'), '1.0.0', false);
		//*wp_enqueue_script('Gmaps', get_bloginfo('template_url') . '/js/gmaps.js', array('jQuery'), '0.2.30', false);
		//*wp_enqueue_script('jQueryValidity', get_bloginfo('template_url') . '/js/jquery.validity.js', array('jQuery'), '1.3.0', false);
		
		//*wp_enqueue_script('Json', get_bloginfo('template_url') . '/js/json2.js', array('jQuery'), '3', false);
		//*wp_enqueue_script('Underscore', get_bloginfo('template_url') . '/js/underscore.js', array('jQuery'), '1.4.3', false);
		
		//**wp_enqueue_script('Hammer', get_bloginfo('template_url') . '/js/hammer.js', array('jQuery'), '0.6.4', false);
		//**wp_enqueue_script('jQuerySpecialeventHammer', get_bloginfo('template_url') . '/js/jquery.specialevent.hammer.js', array('jQuery','Hammer'), '0.6.4', false);
		//**wp_enqueue_script('jQueryHammer', get_bloginfo('template_url') . '/js/jquery.hammer.js', array('jQuery','Hammer'), '0.6.4', false);
		
		//wp_enqueue_script('Angular', get_bloginfo('template_url') . '/js/angular.js', array('jQuery'), '1.0.3', false);
		
		//wp_enqueue_script('ImagesLoaded', get_bloginfo('template_url') . '/js/jquery.imagesloaded.js', array('jQuery'), '2.1.0', false);
		//wp_enqueue_script('EventDrop', get_bloginfo('template_url') . '/js/jquery.event.drop-2.2.js', array('jQuery'), '2.2', false);
		//wp_enqueue_script('AngularAcopeOnReady', get_bloginfo('template_url') . '/js/scope.onready.js', array('jQuery','Angular'), '1.0.3', false);
		//wp_enqueue_script('Amplify', get_bloginfo('template_url') . '/js/amplify.js', array('jQuery'), '1.1.0', false);
		//wp_enqueue_style('JasmineCss', get_bloginfo('template_url') . '/js/jasmine/jasmine.css', null, null, 'all');
		//wp_enqueue_script('Jasmine', get_bloginfo('template_url') . '/js/jasmine/jasmine.js', array('jQuery'), null, false);
		//wp_enqueue_script('JasmineHtml', get_bloginfo('template_url') . '/js/jasmine/jasmine-html.js', array('jQuery'), null, false);
		//wp_enqueue_script('jQuery++', get_bloginfo('template_url') . '/js/jquerypp.js', array('jQuery'), '1.0.0', false);
		//wp_enqueue_script('jScrollPane', get_bloginfo('template_url') . '/js/jquery.jscrollpane.js', array('jQuery'), '1.0.0', false);
		//wp_enqueue_script('Wookmark', get_bloginfo('template_url') . '/js/jquery.wookmark.js', array('jQuery'), '3.0.1', false);
		//wp_enqueue_script('QuoJs', get_bloginfo('template_url') . '/js/quo.js', array('jQuery'), '2.2.0', false);
		//wp_enqueue_script('Environ', get_bloginfo('template_url') . '/js/environ.js', array('jQuery'), '0.6.4', false);
		//wp_enqueue_style('BarajaCss', get_bloginfo('template_url') . '/js/baraja.css', null, null, 'all');
		//wp_enqueue_script('Gridster', get_bloginfo('template_url') . '/js/jquery.gridster.js', array('jQuery'), '1.1.0', false);
		//wp_enqueue_style('GridsterCss', get_bloginfo('template_url') . '/js/jquery.gridster.css', null, null, 'all');
		
		//wp_enqueue_script('Main', get_bloginfo('template_url') . '/js/main.js', array('jQuery', 'Require'), '0.0.1', true);
	}
}

add_action('init', 'ngrecord_init');

function configTheme()
{
?>
<script type="text/javascript">
window.ngrecord = {
	urlTheme:"<?php echo get_bloginfo('template_url');?>"
};
</script>
<?php
}

add_action('wp_head', 'configTheme');

function attachments($attachments)
{
  $args = array(
    // title of the meta box (string)
    'label'         => 'My Attachments',
    // all post types to utilize (string|array)
    'post_type'     => array( 'post', 'page', 'ngrecord_post', 'ngrecord_blog'),
    // allowed file type(s) (array) (image|video|text|audio|application)
    'filetype'      => null,  // no filetype limit
    // include a note within the meta box (string)
    'note'          => 'Attach files here!',
    // text for 'Attach' button in meta box (string)
    'button_text'   => __( 'Attach Files', 'attachments' ),
    // text for modal 'Attach' button (string)
    'modal_text'    => __( 'Attach', 'attachments' ),
    /**
     * Fields for the instance are stored in an array. Each field consists of
     * an array with three keys: name, type, label.
     *
     * name  - (string) The field name used. No special characters.
     * type  - (string) The registered field type.
     *                  Fields available: text, textarea
     * label - (string) The label displayed for the field.
     */
    'fields'        => array(
      array(
        'name'  => 'title',                          // unique field name
        'type'  => 'text',                           // registered field type
        'label' => __( 'Title', 'attachments' ),     // label to display
      ),
      array(
        'name'  => 'caption',                        // unique field name
        'type'  => 'textarea',                       // registered field type
        'label' => __( 'Caption', 'attachments' ),   // label to display
      ),
      array(
        'name'  => 'copyright',                      // unique field name
        'type'  => 'text',                           // registered field type
        'label' => __( 'Copyright', 'attachments' ), // label to display
      ),
    ),
  );

  $attachments->register( 'attachments', $args ); // unique instance name
}

add_action( 'attachments_register', 'attachments' );