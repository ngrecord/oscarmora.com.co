define(['urlTheme/js/main/app'], function(app) {
	App.directive('contentContact', function($timeout, $http, $timeout) {
		return function(scope, element, attrs) {
			element.ready(function() {
				new GMaps({
					div: '#map',
					lat: 6.342512,
					lng: -75.55963
				});

				function validateForm() {
					$.validity.setup({
						outputMode: "label"
					});
					$.validity.start();

					$('#subject, #name, #email, #message').require("Este dato es requerido");

					$('#name, #subject').match(/^[a-záéíóúñ\s]*$/gi, "Por favor no ingrese numeros, simbolos o caracteres especiales");
					$('#email').match('email', "Formato invalido, vuelva a escribir la dirección de correo electronico");
					$('#message').match('nonHtml', "Recuerda no insertar etiquetas, no esta permitido");

					var result = $.validity.end();
					return result.valid;
				}

				$('#dragElement').curtain();
				$('#form form').contactAjax({
					'onSubmit': function(widget) {

						if(!validateForm()) {
							return;
						}

						var params = $(this.element).serializeArray();

						params.push({
							'name': 'action',
							'value': 'contact'
						});

						params.push({
							'name': 'nonce',
							'value': widget.nonce
						});

						var paramsObject = {};

						_.each(params, function(item) {
							paramsObject[item.name] = item.value;
						})

						scope.data = {
							'message': 'Enviando Mensaje de ' + paramsObject.name + '...'
						}

						scope.$digest();

						$http({
							method: 'POST',
							url: widget.action,
							params: paramsObject
						}).
						success(function(data, status, headers, config) {
							scope.data = {
								'message': 'Su mensaje a sido enviado...'
							}

							widget.element.trigger('reset');

							$timeout(function() {
								delete scope.data.message;
							}, 5000);
						}).
						error(function(data, status, headers, config) {
							scope.data = {
								'message': 'No a sido posible enviar su mensaje...'
							}

							$timeout(function() {
								delete scope.data.message;
							}, 5000);
						});
					},
					'onReset': function(widget) {
						$(this.element).find('label.error').remove();
						return;
					}
				});

				if(isMobile){
					$('.mask-contact').on('swipe', function(event) {
						if(event.direction == 'up'){
							$(this).children('div').trigger("mousewheel", [1,0,1])
						}else if(event.direction == 'down'){
							$(this).children('div').trigger("mousewheel", [-1,0,-1])
						}

						event.preventDefault();

						$('#applicationContent').off('tap');
						return false;
					});
				}
			})
		}
	});

	App.directive('itemContact', function($timeout, $http, $timeout) {
		return function(scope, element, attrs) {
			element.ready(function() {
				$('#twitter').sharrre({
					share: {
						twitter: true
					},
					template: '<a class="twitter"><div class="count" href="#">{total}</div><div class="title">Tweet</div></a>',
					enableHover: false,
					enableTracking: true,
					buttons: {
						twitter: {
							via: '0scarMora'
						}
					},
					click: function(api, options) {
						api.simulateClick();
						api.openPopup('twitter');

						return false;
					}
				});
				$('#facebook').sharrre({
					share: {
						facebook: true
					},
					template: '<a class="facebook"><div class="count" href="#">{total}</div><div class="title">Like</div></a>',
					enableHover: false,
					enableTracking: true,
					click: function(api, options) {
						api.simulateClick();
						api.openPopup('facebook');

						return false;
					}
				});
			})
		}
	});

	App.directive('contentAbout', function($timeout, $http, $timeout) {
		return function(scope, element, attrs) {

			element.ready(function() {
				var bookblock = $('#about').bookblock({
					speed: 1000,
					easing: 'ease-in-out',
					shadows: true,
					shadowSides: 0.2,
					shadowFlip: 0.1,
					perspective: 1300,
					circular: true,
					nextEl: '.iconNextPost',
					prevEl: '.iconPrevPost',
					autoplay: false,
					interval: 3000,
					keyboard: true,
					onEndFlip: function(old, page, isLimit) {
						return false;
					},
					onBeforeFlip: function(page) {
						return false;
					}
				})

				if(isMobile){
					$('#applicationContent').on('swipe', function(event) {
						event.preventDefault();
						
						if(event.direction == 'left'){
							bookblock.prev()
						}else if(event.direction == 'right'){
							bookblock.next()
						}

						$('#applicationContent').off('tap');
						return false;
					});
				}

				scope.$watch(function() {
					bookblock.current = 0;
					bookblock.update();
				})
			})
		}
	});

	App.directive('itemAbout', function($timeout, $http) {
		return function(scope, element, attrs) {

			element.ready(function(){
				if(isMobile){
					$('#applicationContent').on('swipe', function(event) {
						event.preventDefault();

						if(event.direction == 'up'){
							element.trigger("mousewheel", [1,0,1])
						}else if(event.direction == 'down'){
							element.trigger("mousewheel", [-1,0,-1])
						}

						$('#applicationContent').off('tap');
					});
				};
			});

			element.mousewheel(function(event, delta, deltaX, deltaY) {
				var scrollBackup = this.scrollTop;

				$(this).animate({
					scrollTop: "-=" + (delta * (isMobile?120:60))
				}, {
					duration: 'slow',
					queue: false,
					easing: 'easeOutBack',
					step: function(now, fx) {
						var data = fx.elem.id + ' ' + fx.prop + ': ' + now;
						//trace(data);
					},
					complete: function() {
						if(scrollBackup != 0 && scrollBackup == this.scrollTop) {
							scope.update();
						}
					}
				});

				event.preventDefault();

				return false;
			});
		}
	});

	App.directive('contentBlog', function($timeout, $http) {
		return function(scope, element, attrs) {

			element.ready(function() {
				$('#blog ul').masonry({
					isResizable: true,
					isAnimated: true,
					animationOptions: {
						queue: false,
						duration: 700
					},
					gutterWidth: 0,
					isRTL: false,
					isFitWidth: false,
					containerStyle: {
						position: 'absolute'
					}
				});

				if(isMobile){
					$('#applicationContent').on('swipe', function(event) {
						if(event.direction == 'up'){
							
							element.trigger("mousewheel", [1,0,1])
						}else if(event.direction == 'down'){
							
							element.trigger("mousewheel", [-1,0,-1])
						}

						event.preventDefault();

						$('#applicationContent').off('tap');
						return false;
					});
				}
			})

			scope.$watch('itemData', function(e) {
				if(_.isUndefined(e)) {
					$timeout(function() {
						$('#blog ul').masonry('reloadItems');
						$('#blog ul').masonry('reload');
						$('#blog ul').trigger("resize");
					}, 100);
				}
			})

			element.mousewheel(function(event, delta, deltaX, deltaY) {
				var scrollBackup = this.scrollTop;

				$(this).animate({
					scrollTop: "-=" + (delta * 300)
				}, {
					duration: 'slow',
					queue: false,
					easing: 'easeOutBack',
					step: function(now, fx) {
						var data = fx.elem.id + ' ' + fx.prop + ': ' + now;
						//trace(data);
					},
					complete: function() {
						if(scrollBackup != 0 && scrollBackup == this.scrollTop) {
							scope.update();
						}
					}
				});

				event.preventDefault();
			});
		}
	});

	App.directive('itemBlog', function($timeout, $http, $document) {
		return function(scope, element, attrs) {

			scope.post.style = {
				'display': 'none'
			};

			element.ready(function() {
				$(element).find("img").load(function(e) {
					var style = {
						'display': 'block'
					}

					scope.post.style = style;

					scope.$digest();

					$timeout(function() {
						$('#blog ul').masonry('reloadItems');
						$('#blog ul').masonry('reload');
						$('#blog ul').trigger("resize");
					}, 100);
				});
			})
		}
	});

	App.directive('titleBlog', function() {
		return {
			priority: 0,
			replace: false,
			transclude: true,
			compile: function(tElement, tAttrs, transclude) {
				return function preLink(scope, iElement, iAttrs, controller) {
					var sizeMaxChar = 33;
					var arrayWord = scope.post.title.split(" ");
					var arrayText = [];
					var result = "";

					_.each(arrayWord, function(word) {
						var i = arrayText.length - 1;

						if(_.isUndefined(arrayText[i])) {
							arrayText.push(word);
						} else {
							if(arrayText[i].length + (word.length + 1) < sizeMaxChar) {
								arrayText[i] = arrayText[i] + " " + word;
							} else {
								arrayText.push(word);
							}
						}
					})

					_.each(arrayText, function(text) {
						result += "<h1>" + $.trim(text) + "</h1>";
					})

					$(iElement).html(result);
				};
			}
		};
	});

	App.directive('dateBlog', function() {
		return {
			priority: 0,
			replace: false,
			transclude: true,
			compile: function(tElement, tAttrs, transclude) {
				return function preLink(scope, iElement, iAttrs, controller) {
					var result = "<span>" + moment(scope.post.date).format('LL'); + "</span>";
					$(iElement).html(result);
				};
			}
		};
	});

	App.directive('blogItemInfo', function($timeout, dateFilter) {
		return function(scope, element, attrs) {

			element.ready(function(){
				if(isMobile){
					$('#itemContent').on('swipe', function(event) {
						if(event.direction == 'up'){
							element.trigger("mousewheel", [1,0,1])
						}else if(event.direction == 'down'){
							element.trigger("mousewheel", [-1,0,-1])
						}

						event.preventDefault();

						$('#applicationContent').off('tap');
						return false;
					});
				}
			})

			element.mousewheel(function(event, delta, deltaX, deltaY) {

				$(this).animate({
					scrollTop: "-=" + (delta * 90)
				}, {
					duration: 'slow',
					queue: false,
					easing: 'easeOutBack'
				});

				event.preventDefault();
			});
		}
	});

	App.directive('contentPortfolio', function($timeout, $http, dataResource) {
		return function(scope, element, attrs) {

			element.ready(function(){
				if(isMobile){
					$('#applicationContent').on('swipe', function(event) {

						if(event.direction == 'left'){
							element.trigger("mousewheel", [-1,0,-1])
						}else if(event.direction == 'right'){
							element.trigger("mousewheel", [1,0,1])
						}

						event.preventDefault();

						$('#applicationContent').off('tap');
						return false;
					});
				}
			});

			element.mousewheel(function(event, delta, deltaX, deltaY) {
				var scrollBackup = this.scrollLeft;

				$(this).animate({
					scrollLeft: "-=" + (delta * 300)
				}, {
					duration: 'slow',
					queue: false,
					easing: 'easeOutBack',
					step: function(now, fx) {
						var data = fx.elem.id + ' ' + fx.prop + ': ' + now;
						//trace(data);
					},
					complete: function() {
						if(scrollBackup != 0 && scrollBackup == this.scrollLeft || scrollBackup == 0 && this.scrollLeft == 0 && delta == -1) {
							scope.update();
						}
					}
				});

				event.preventDefault();
			});
		}
	});

	App.directive('thumbnailPortafolio', function($timeout, $http, dataResource) {
		return function(scope, element, attrs) {
			element.ready(function() {
				var image = element.find("#image2");
				element.hover(

				function() {
					image.animate({
						opacity: "1"
					}, {
						duration: 2000,
						queue: false,
						easing: 'easeOutBack'
					});
				}, function() {
					image.animate({
						opacity: "0"
					}, {
						duration: 2000,
						queue: false,
						easing: 'easeOutBack'
					});
				});
			});
		}
	});

	App.directive('titlePortfolio', function() {
		return {
			priority: 0,
			replace: false,
			transclude: true,
			compile: function(tElement, tAttrs, transclude) {
				return function preLink(scope, iElement, iAttrs, controller) {
					var sizeMaxChar = 33;
					var arrayWord = scope.post.title.split(" ");
					var arrayText = [];
					var result = "";

					_.each(arrayWord, function(word) {
						var i = arrayText.length - 1;

						if(_.isUndefined(arrayText[i])) {
							arrayText.push(word);
						} else {
							if(arrayText[i].length + (word.length + 1) < sizeMaxChar) {
								arrayText[i] = arrayText[i] + " " + word;
							} else {
								arrayText.push(word);
							}
						}
					})

					_.each(arrayText, function(text) {
						result += "<p>" + $.trim(text) + "</p>";
					})

					$(iElement).html(result);
				};
			}
		};
	});

	App.directive('datePortfolio', function() {
		return {
			priority: 0,
			replace: false,
			transclude: true,
			compile: function(tElement, tAttrs, transclude) {
				return function preLink(scope, iElement, iAttrs, controller) {
					var result = "<p>" + moment(scope.post.date).format('LL'); + "</p>";
					$(iElement).html(result);
				};
			}
		};
	});

	App.directive('portfolioPreview', function($timeout, dateFilter) {
		return function(scope, element, attrs) {

		}
	});

	App.directive('portfolioItemInfo', function($timeout, dateFilter) {
		return function(scope, element, attrs) {

			element.ready(function(){
				if(isMobile){
					$('#itemInfo').on('swipe', function(event) {
						if(event.direction == 'up'){
							element.trigger("mousewheel", [1,0,1])
						}else if(event.direction == 'down'){
							element.trigger("mousewheel", [-1,0,-1])
						}

						event.preventDefault();

						$('#applicationContent').off('tap');
						return false;
					});
				}
			})

			element.mousewheel(function(event, delta, deltaX, deltaY) {
				$(this).animate({
					scrollTop: "-=" + (delta * 60)
				}, {
					duration: 'slow',
					queue: false,
					easing: 'easeOutBack'
				});

				event.preventDefault();
			});
		}
	});

	App.directive('portfolioItemGallery', function($timeout, dateFilter) {
		return function(scope, element, attrs) {

			var $el, baraja;

			element.ready(function(){
				if(isMobile){
					$('#itemGallery').on('swipe', function(event) {
						if(event.direction == 'left'){
							baraja.previous();
						}else if(event.direction == 'right'){
							baraja.next();
						}

						event.preventDefault();

						$('#applicationContent').off('tap');
						return false;
					});
				}
			})

			scope.$watch('itemData.gallery', function(e) {
				if(!_.isUndefined(e)) {
					$el = $('#itemGallery ul');
					baraja = $el.baraja();

					$('.iconPrevImage').on('click', function(event) {
						baraja.previous();
					});

					$('.iconNextImage').on('click', function(event) {
						baraja.next();
					});

					/*baraja.fan({
						speed: 500,
						easing: 'ease-out',
						range: 90,
						direction: 'right',
						origin: {
							x: 25,
							y: 100
						},
						center: true,
						scatter : true
					});*/

					baraja._setDefaultFanSettings({
						speed: 500,
						easing: 'ease-out',
						range: 20,
						direction: 'right',
						origin: {
							x: 50,
							y: 200
						},
						center: true,
						translation: 300
					});

					var loadingGallery = element.find('#loadingGallery');
					loadingGallery.css('display', 'block');

					loadingGallery.animate({
						opacity: 1
					}, {
						duration: 'slow',
						queue: false,
						easing: 'easeOutBack'
					});

					$(element).find("img").load(function(e) {
						loadingGallery.animate({
							opacity: 0
						}, {
							duration: 'slow',
							queue: false,
							complete: function() {
								loadingGallery.css('display', 'none');
							}
						});
					});
				}
			});
		}
	});

	App.directive('compile', function($compile) {
		return function(scope, element, attrs) {
			scope.$watch(

			function(scope) {
				return scope.$eval(attrs.compile);
			}, function(value) {
				element.html(value);
				$compile(element.contents())(scope);
			});
		};
	})
});