function contactController($scope, dataResource, $http, $route, $routeParams, $location) {}

function aboutController($scope, dataResource, $http, $route, $routeParams, $location) {
	$scope.init = function() {
		dataResource.config({
			//showPageUrl:true
		});
	}

	$scope.update = function() {
		if(_.isUndefined($scope.data)) {

			$scope.load = false;

			$http({
				method: 'GET',
				url: dataResource.get_page_index()
			}).
			success(function(data, status) {
				$scope.data = data;
				$scope.load = true;

				dataResource.loaded(data);
			})
		}
	}

	$scope.open = function(page) {
		$scope.itemData = page;
		$scope.index = _.indexOf($scope.data.pages, page);
	}

	$scope.close = function() {
		delete $scope.itemData;
	}

	$scope.nextPage = function() {
		if(($scope.index + 1) < $scope.data.pages.length) {
			$scope.index++;
			$scope.itemData = $scope.data.pages[$scope.index]
		} else {
			$scope.update()
		}
	}

	$scope.prevPage = function() {
		if(($scope.index - 1) >= 0) {
			$scope.index--;
			$scope.itemData = $scope.data.pages[$scope.index]
		}
	}

	$scope.init();
	$scope.update();
}

function blogController($scope, dataResource, $http, $route, $routeParams, $location) {
	$scope.init = function() {
		dataResource.config({
			post_type: 'ngrecord_blog',
			count: 5,
			page: _.isUndefined($location.search().page) ? 1 : parseInt($location.search().page),
			//showPageUrl:true
		});
	}

	$scope.update = function() {
		if(_.isUndefined($scope.data)) {

			$scope.load = false;

			$http({
				method: 'GET',
				url: dataResource.get_recent_posts()
			}).
			success(function(data, status) {
				$scope.data = data;

				$scope.load = true;
				dataResource.loaded(data);
			})
		} else {
			if($scope.data.pages == dataResource.options.page || !$scope.load) {
				return true;
			}

			$scope.load = false;

			dataResource.options.page++;

			$http({
				method: 'GET',
				url: dataResource.get_recent_posts()
			}).
			success(function(data, status) {
				_.each(data.posts, function(post) {
					$scope.data.posts.push(post);
				}, this);

				$scope.load = true;
				dataResource.loaded(data);
			})
		}
	}

	$scope.open = function(post) {
		$scope.itemData = post;
		$scope.index = _.indexOf($scope.data.posts, post);
	}

	$scope.close = function() {
		delete $scope.itemData;
	}

	$scope.nextPost = function() {
		if(($scope.index + 1) < $scope.data.posts.length) {
			$scope.index++;
			$scope.itemData = $scope.data.posts[$scope.index]
		} else {
			$scope.update()
		}
	}

	$scope.prevPost = function() {
		if(($scope.index - 1) >= 0) {
			$scope.index--;
			$scope.itemData = $scope.data.posts[$scope.index]
		}
	}

	$scope.init();
	$scope.update();
}

function portfolioController($scope, dataResource, $http, $route, $routeParams, $location) {

	$scope.init = function() {
		dataResource.config({
			post_type: 'ngrecord_post',
			count: 5,
			page: _.isUndefined($location.search().page) ? 1 : parseInt($location.search().page),
			//showPageUrl:true
		});
	}

	$scope.update = function(fn) {
		if(_.isUndefined($scope.data)) {

			$scope.load = false;

			$http({
				method: 'GET',
				url: dataResource.get_recent_posts()
			}).
			success(function(data, status) {
				$scope.data = data;

				$scope.load = true;
				dataResource.loaded(data);
			})
		} else {
			if($scope.data.pages == dataResource.options.page || !$scope.load) {
				return true;
			}

			$scope.load = false;

			dataResource.options.page++;

			$http({
				method: 'GET',
				url: dataResource.get_recent_posts()
			}).
			success(function(data, status) {
				_.each(data.posts, function(post) {
					$scope.data.posts.push(post);
				}, this);

				$scope.load = true;
				dataResource.loaded(data);

				if(!_.isUndefined(fn)) {
					fn();
				}
			})
		}
	}

	$scope.open = function(post) {
		$scope.itemData = post;
		$scope.index = _.indexOf($scope.data.posts, post);

		$scope.loadGallery($scope.itemData, $scope.index);
	}

	$scope.loadGallery = function(post, index) {
		if(!_.isUndefined(post.gallery)) {
			return;
		}

		$http({
			method: 'GET',
			url: dataResource.get_attachment(post.id)
		}).
		success(function(data, status, headers, config) {
			if(!_.isUndefined(data.attachments)) {
				$scope.data.posts[index].gallery = data.attachments;
			}

			//dataResource.loaded(data);
		}).
		error(function(data, status, headers, config) {

		});
	}

	$scope.close = function() {
		delete $scope.itemData
	}

	$scope.viewInfo = function() {
		if(_.isUndefined($scope.info)) {
			$scope.info = true;
			$scope.styleItem = {
				width: '100%'
			}
		} else {
			delete $scope.info;
			delete $scope.styleItem;
		}
	}

	$scope.nextPost = function() {
		if(($scope.index + 1) < $scope.data.posts.length) {
			$scope.index++;
			$scope.itemData = $scope.data.posts[$scope.index]
			$scope.loadGallery($scope.itemData, $scope.index);
		} else {
			$scope.update($scope.nextPost)
		}

	}

	$scope.prevPost = function() {
		if(($scope.index - 1) >= 0) {
			$scope.index--;
			$scope.itemData = $scope.data.posts[$scope.index]
			$scope.loadGallery($scope.itemData, $scope.index);
		}
	}

	$scope.init();
	$scope.update();
}