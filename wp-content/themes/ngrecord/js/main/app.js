define(['angular', 'urlTheme/js/main/controllers'], function(angular, controllers) {
	window.App = angular.module('app', []);
	
	App.config(function($routeProvider, $locationProvider) {
		$locationProvider.html5Mode(false).hashPrefix('!');

		$routeProvider.
		when('/', {
			redirectTo: '/about/'
		}).
		when('/portfolio/', {
			templateUrl: window.ngrecord.urlTheme + '/js/tmpl/portfolio.html',
			controller: portfolioController,
			reloadOnSearch: false
		}).
		when('/blog/', {
			templateUrl: window.ngrecord.urlTheme + '/js/tmpl/blog.html',
			controller: blogController,
			reloadOnSearch: false
		}).
		when('/about/', {
			templateUrl: window.ngrecord.urlTheme + '/js/tmpl/about.html',
			controller: aboutController,
			reloadOnSearch: false
		}).
		when('/contact/', {
			templateUrl: window.ngrecord.urlTheme + '/js/tmpl/contact.html',
			controller: contactController,
			reloadOnSearch: false
		}).
		otherwise({
			redirectTo: '/about/'
		});
	});
});