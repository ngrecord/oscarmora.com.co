define(['urlTheme/js/main/app'], function(app) {
	App.factory('dataResource', function($location, $rootScope) {
		return {
			config: function(options) {
				_.extend(this.options, options);
				return this;
			},
			options: {
				get_recent_posts: true,
				get_post: false,
				get_page: false,
				get_date_posts: false,
				get_category_posts: false,
				get_tag_posts: false,
				get_author_posts: false,
				get_search_results: false,
				get_date_index: false,
				get_category_index: false,
				get_tag_index: false,
				get_author_index: false,
				get_nonce: false,
				post_id: null,
				page_id: null,
				category_id: null,
				tag_id: null,
				author_id: null,
				post_slug: null,
				page_slug: null,
				category_slug: null,
				tag_slug: null,
				author_slug: null,
				date: null,
				search: null,
				post_type: null,
				page: 1,
				count: 10,
				showPageUrl: false
			},
			get_recent_posts: function() {
				var url = '?json=get_recent_posts';

				url += _.isNull(this.options.page) ? '' : (_.isNumber(this.options.page) ? '&page=' + this.options.page : '')
				url += _.isNull(this.options.post_type) ? '' : (_.isString(this.options.post_type) ? '&post_type=' + this.options.post_type : '')
				url += _.isNull(this.options.count) ? '' : (_.isNumber(this.options.count) ? '&count=' + this.options.count : '')

				this.load();

				return url;
			},
			get_page_index: function() {
				var url = '?json=get_page_index';

				this.load();

				return url;
			},
			get_attachment: function(id) {
				var url = '?json=attachment.get';
				url += _.isUndefined(id) ? '' : (_.isNumber(id) ? '&id=' + id : '');

				//this.load();

				return url;
			},
			load: function() {
				var loading = $('#loading');
				loading.css('display', 'block');

				loading.animate({
					opacity: 1
				}, {
					duration: 'slow',
					queue: false,
					easing: 'easeOutBack'
				});
			},
			loaded: function(data) {
				var loading = $('#loading');

				loading.animate({
					opacity: 0
				}, {
					duration: 'slow',
					queue: false,
					easing: 'easeOutBack',
					step: function(now, fx) {
						var data = fx.elem.id + ' ' + fx.prop + ': ' + now;
						//trace(data);
					},
					complete: function() {
						loading.css('display', 'none');
					}
				});

				if(this.options.showPageUrl) {
					$location.search('page', (this.options.page));
				}
			}
		};
	});
});