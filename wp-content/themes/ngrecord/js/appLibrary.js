;(function($) {
	"use strict";

	var defaults = {
		'select': '.mask-contact',
		'dragElement': '#dragElement',
		'refWidth': window
	};

	function Curtain(element, options) {
		var widget = this;

		widget.element = element;
		widget.config = $.extend({}, defaults, options);
		widget.dragElement = $(widget.config.dragElement);
		widget.dragItemContent = $(widget.config.select);
		widget.refWidth = $(widget.config.refWidth).width();

		$(widget.config.refWidth).on('resize', function() {
			widget.refWidth = $(widget.config.refWidth).width();
			widget.dragElement.css({
				left: '50%'
			});

			widget.render({
				'offsetX': widget.refWidth * 0.5
			});
		})

		widget.render = function(prop) {
			widget.dragItemContent.each(function() {
				$(this).children().each(function() {
					$(this).css('width', widget.refWidth);

					$(this).mousewheel(function(event, delta, deltaX, deltaY) {
						var scrollBackup = this.scrollTop;

						$(this).animate({
							scrollTop: "-=" + (delta * 90)
						}, {
							duration: 'slow',
							queue: false,
							easing: 'easeOutBack',
							step: function(now, fx) {
								var data = fx.elem.id + ' ' + fx.prop + ': ' + now;
								//trace(data);
							},
							complete: function() {}
						});

						event.preventDefault();

						return false;
					});
				})
			})

			if(prop.offsetX < widget.refWidth && prop.offsetX > 0) {
				var numBase = prop.offsetX;

				widget.dragElement.css({
					left: numBase + widget.dragElement.width()
				});

				$(widget.dragItemContent.get(0)).css("width", numBase);
				$(widget.dragItemContent.get(1)).css("width", (widget.refWidth - numBase)).children("div").css("left", -numBase);
			}
		}

		this.init();
	};

	Curtain.prototype.init = function() {

		var _this = this;
		_this.render({
			'offsetX': _this.refWidth * 0.5
		});

		if(!isMobile){
			this.dragElement.drag(function(ev, dd) {
				_this.render(dd);
				return true;
			});
		}else{
			this.dragElement.on('dragstart drag dragend', function(event) {
				event.preventDefault();

				var eventData;

				if(event.direction == "right"){
					eventData = {'offsetX':$('#dragElement').position().left + event.distance}
				}else if(event.direction == "left"){
					eventData = {'offsetX':$('#dragElement').position().left - event.distance}
				}

				_this.render(eventData);

				$(this).off('tap');
				$('#applicationContent').off('swipe');
			});
		}
	};

	$.fn.curtain = function(options) {
		new Curtain(this.first(), options)

		return this.first();
	}
}(jQuery))

;(function($) {
	"use strict";

	var defaults = {
		'action': "/wp-admin/admin-ajax.php",
		'onSubmit': function() {},
		'onReset': function() {}
	};

	function ContactAjax(element, options) {
		var widget = this;

		widget.element = element;
		widget.config = $.extend({}, defaults, options);
		widget.action = widget.config.action;
		widget.onSubmit = widget.config.onSubmit;
		widget.onReset = widget.config.onReset;

		$(widget.element).on('submit', function() {
			widget.onSubmit(widget);
		});

		$(widget.element).on('reset', function() {
			widget.onReset(widget);
		})

		widget.certNonce = function() {
			jQuery.ajax({
				type: "POST",
				dataType: "JSON",
				url: widget.action,
				data: {
					action: "contact_nonce"
				},
				complete: function(response) {
					var data = jQuery.parseJSON(response.responseText);
					widget.nonce = data.nonce;
				}
			})
		}

		widget.certNonce();

		this.init();
	};

	ContactAjax.prototype.init = function() {

	};

	$.fn.contactAjax = function(options) {
		new ContactAjax(this.first(), options)

		return this.first();
	}
}(jQuery))

trace = function(args) {
	console.log("\n###########");
	console.log(args);
	console.log("###########\n");
};

traceJSON = function(args) {
	console.log("\n###########");
	console.log(JSON.stringify(args));
	console.log("###########\n");
};