requirejs.config({
	paths: {
		urlTheme: window.ngrecord.urlTheme,
		angular: window.ngrecord.urlTheme + '/js/angular.min',
		jquery: window.ngrecord.urlTheme + '/js/jquery-1.8.3.min'
	},
	shim: {
		angular: {
			exports: 'angular'
		}
	}
});

/*
var lastRoute = $route.current;
$scope.$on('$locationChangeSuccess', function(event) {
    $route.current = lastRoute;
});
*/

;
(function() {
	window.detectMobile = function() {
		if(UA.isMobile()) {
			return true;
		}

		if(UA.isTablet()) {
			return true;
		}

		if(UA.isAndroid()) {
			return true;
		}

		if(UA.isIOS()) {
			return true;
		}

		if(UA.isIPad()) {
			return true;
		}
		if(UA.isIPhone()) {
			return true;
		}
		if(UA.isIPod()) {
			return true;
		}

		if(UA.isKindle()) {
			return true;
		}

		return false;
	}
}());

var _init = function() {
		jQuery(document).ready(function($) {
			'use strict';

			if(UA.isIE()) {
				var main = $("#main");
				main.css('display', 'none');

				var body = $("body");
				body.append("<div class='alert-box alert' style='text-align:center; width:80%; margin:auto; color:#fff; font-family:\"arial\";'>Esta página no es soportada por el navegador Internet Explorer; por favor utilize <a style='color:#000;' href='http://www.google.com/intl/es/chrome/browser/'>Google Chrome</a><br> o <a style='color:#000;' href='http://www.mozilla.org/es-ES/firefox/new/'>Mozilla</a> para poderla visualizar...</div>");
				return false;
			}

			window.isMobile = detectMobile();

			requirejs(
			['urlTheme/js/appLibrary', 'urlTheme/js/lib/appLib', 'urlTheme/js/lib/appResource'], function() {
				requirejs(
				['angular', (isMobile ? 'urlTheme/js/lib/appMobile' : ''), 'urlTheme/js/main/controllers', 'urlTheme/js/main/app', 'urlTheme/js/main/directives', 'urlTheme/js/main/services'], function(angular, app) {
					angular.bootstrap(document, ['app']);
				})
			});

			$('.menu-item a').click(function() {
				var itemLi = $(this).parent();
				var itemIndex = itemLi.index();

				var menuMovil = $('#menuMovil ul');
				var menuScreen = $('#menuScreen ul');

				menuMovil.find('.active').removeClass('active');
				menuScreen.find('.active').removeClass('active');

				menuMovil.children("li:eq(" + itemIndex + ")").addClass('active');
				menuScreen.children("li:eq(" + itemIndex + ")").addClass('active');
			});

			function info($el, $class, $msg) {
				$el.append("<div id='" + $class + "' class='" + $class + "' style='opacity:0;'><div><p>" + $msg + "</p><span></span></div></div>");
				var info = $el.find('#' + $class);
				
				info.animate({
					opacity: 1
				}, {
					duration: 'slow',
					queue: true,
					complete: function() {
						info.delay(5000).animate({
							opacity: 0
						}, {
							duration: 'slow',
							queue: true,
							complete: function() {
								info.remove();
							}
						});
					}
				});
			}

			if(isMobile) {
				info($('body'), 'infoMobile', "Swipe (Up-Down) - Swipe (Left-Right) - Drag");
			} else {
				info($('body'), 'infoScreen', "Scroll (Up-Down) - Scroll (Left-Right) - Drag");
			}
		});
	}

if(!window.jQuery) {
	requirejs(['jquery'], function() {
		_init();
	})
} else {
	_init();
}