<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<meta name="google" value="notranslate">
<link rel="icon" type="image/gif" href="<?php bloginfo('template_url');?>/img/iconWeb.gif" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );

	?>
</title>

<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37822783-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
</head>

<body class="">
	<header id="header">
		<div id="menuMovil">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
				<span class="logo"></span>
			</a>
			
			<nav id="access" role="navigation">
				<input type="checkbox" id="menu">
				<label for="menu" onclick></label>
				<ul>
					<li id="menu-about" class="menu-item"><a href="#!/about"><p>Sobre Mí</p></a></li>
					<li id="menu-portfolio" class="menu-item"><a href="#!/portfolio"><p>Portafolio</p></a></li>
					<li id="menu-blog" class="menu-item"><a href="#!/blog"><p>Blog</p></a></li>
					<li id="menu-contact" class="menu-item"><a href="#!/contact"><p>contacto</p></a></li>
				</ul>		 
			</nav>
		</div>

		<div id="menuScreen">
			<nav id="access" role="navigation">
				<ul>
					<li id="menu-about" class="menu-item"><a href="#!/about"><p class="iconAbout">Sobre Mí</p></a></li>
					<li id="menu-portfolio" class="menu-item"><a href="#!/portfolio"><p class="iconPortfolio">Portafolio</p></a></li>
					<li id="menu-blog" class="menu-item"><a href="#!/blog"><p class="iconBlog">Blog</p></a></li>
					<li id="menu-contact" class="menu-item"><a href="#!/contact"><p class="iconContact">contacto</p></a></li>
				</ul>		 
			</nav>
		</div>
	</header>
	
	<div id="main">