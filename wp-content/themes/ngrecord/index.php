<?php get_header(); ?>

<div id="content">
  <ng:view></ng:view>
</div>
<div id="loading" style="display:none; opacity:0;"><span><div class="msgLoading">Cargando Recursos...</div></span></div>
<?php get_footer(); ?>  